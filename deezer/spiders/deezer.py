from __future__ import division, absolute_import, unicode_literals
from scrapy import Request, Spider
import re
import json
import traceback


class DeezerSpider(Spider):
    name = "deezer"

    SEARCH_URL = 'https://www.deezer.com/search/{search_term}'
    USER_TOKEN_URL = 'https://www.deezer.com/ajax/gw-light.php?method=deezer.getUserData&input=3&api_version=1.0&api_token=&cid=407957440'
    PRODUCT_API_URL = 'https://www.deezer.com/ajax/gw-light.php?method=artist.getTopTrack&input=3&api_version=1.0&api_token={user_token}&cid=116991676'

    def __init__(self,
                 search_term=None,
                 product_url=None,
                 *args, **kwargs):
        super(DeezerSpider, self).__init__(*args, **kwargs)

        self.product_url = product_url if product_url else None
        self.search_term = search_term if search_term else None

    def start_requests(self):
        if self.product_url:
            yield Request(url=self.product_url, callback=self.parse_fan)
        if self.search_term:
            yield Request(url=self.SEARCH_URL.format(search_term=self.search_term), callback=self.parse_search)

    def parse_fan(self, response):
        fan = re.search(r'"NB_FAN":(\d+),', response.body)
        fan = fan.group(1) if fan else None
        return Request(url=self.USER_TOKEN_URL, callback=self.parse_user_token, meta={
            'fan': fan
        })

    def parse_art_id(self, url):
        art_id = re.search(r'/artist\/(\d+)\/top_track', url)
        return art_id.group(1) if art_id else None

    def parse_user_token(self, response):
        try:
            data = json.loads(response.body)
        except:
            print 'Error in parsing the user token: {}'.format(traceback.format_exc())
        else:
            user_token = data.get('results', {}).get('checkForm', '')
            meta = response.meta.copy()
            meta['user_token'] =  user_token
            if self.product_url:
                art_id = self.parse_art_id(self.product_url)
                meta['art_id'] = art_id
                yield Request(
                    url=self.PRODUCT_API_URL.format(user_token=user_token),
                    method='POST',
                    body=json.dumps({
                        "art_id": art_id,
                        "start": 0,
                        "nb": 2000
                    }),
                    meta=meta,
                    callback=self.parse_product
                )

    def parse_product(self, response):
        try:
            data = json.loads(response.body)
            data = data.get('results', {})
        except:
            print 'Error in parsing product json: {}'.format(traceback.format_exc())
        else:
            total = data.get('total')
            next = data.get('next')
            if total > next:
                yield response.request.replace(body=json.dumps({
                    "art_id": response.meta.get('art_id'),
                    "start": next - 1,
                    "nb": 2000
                }), dont_filter=True)
            for idx, datum in enumerate(data.get('data', [])):
                item = {
                    "No": idx + 1,
                    "Track": datum.get('SNG_TITLE', ''),
                    "ALBUM": datum.get('ALB_TITLE', ''),
                    "Art Name": datum.get('ART_NAME', ''),
                    "Popularity": '{:.2f} / 10'.format(float(datum.get('RANK_SNG', 0.00)) / 100000),
                    "Fan": int(response.meta.get('fan'))
                }
                yield item

    def parse_search(self, response):
        data = response.xpath('//script[contains(text(), "window.__DZR_APP_STATE__ = ")]/text()').extract()
        if not data:
            return
        data = data[0].replace('window.__DZR_APP_STATE__ = ', '')
        try:
            data = json.loads(data)
        except:
            print 'Error parsing search: {}'.format(traceback.format_exc())
        else:
            item = {
                'ARTIST': {
                    'COUNT': data.get('ARTIST', {}).get('total', 0),
                    'TITLE':  data.get('ARTIST', {}).get('data', [])[0].get('ART_NAME') if data.get('ARTIST', {}).get('data', []) else None,
                    'Fans': data.get('ARTIST', {}).get('data', [])[0].get('NB_FAN') if data.get('ARTIST', {}).get('data', []) else None,
                    'URL': 'https://www.deezer.com/de/artist/{}/top_track'.format(
                        data.get('ARTIST', {}).get('data', [])[0].get('ART_ID') if data.get('ARTIST', {}).get('data',
                                                                                                              []) else None,
                    )
                },
                'ALBUM': {
                    'COUNT': data.get('ALBUM', {}).get('total'),
                    'TITLE': data.get('ALBUM', {}).get('data', [])[0].get('ALB_TITLE', '') if data.get('ALBUM', {}).get('data', []) else None,
                    'ART': data.get('ALBUM', {}).get('data', [])[0].get('ART_NAME', '') if data.get('ALBUM', {}).get('data', []) else None
                },
                'TRACK': {
                    'COUNT': data.get('TRACK', {}).get('total'),
                    'TITLE': data.get('TRACK', {}).get('data', [])[0].get('SNG_TITLE', '') if data.get('TRACK', {}).get('data', []) else None,
                    'ART': data.get('TRACK', {}).get('data', [])[0].get('ART_NAME', '') if data.get('TRACK', {}).get('data', []) else None
                },
                'PLAYLIST': {
                    'COUNT': data.get('PLAYLIST', {}).get('total'),
                    'TITLE': data.get('PLAYLIST', {}).get('data', [])[0].get('TITLE', '') if data.get('PLAYLIST', {}).get('data', []) else None,
                },
                'PODCAST': {
                    'COUNT': data.get('SHOW', {}).get('total'),
                    'TITLE': data.get('SHOW', {}).get('data', [])[0].get('SHOW_NAME', '') if data.get('SHOW', {}).get('data', []) else None,
                }
            }
            return item